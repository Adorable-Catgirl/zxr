#!/usr/bin/env lua
function check_func(name)
	local res = load("return "..name, "=loaded")()
	print("FUNC", name, (res and "OK") or "nil")
	if not res then
		io.stderr:write(name.." does not exist, exiting...\n")
		os.exit(-1)
	end
	return res
end

function check_lib(name)
	local res = pcall(require, name)
	print("LIB", name, (res and "OK") or "nil")
	if not res then
		io.stderr:write(name.." not found, exiting...\n")
		os.exit(-1)
	end
	return res
end

print("Checking Lua enviroment...")

check_func("table.concat")
check_func("require")
check_func("os.execute")
check_lib("io")
check_func("io.popen")
--Start of custom checks


--End of custom checks

if (not os.execute("whereis -V>/dev/null")) then
	io.stderr:write("whereis does not exist (how???), exiting...\n")
	os.exit(-1)
end

function exec(com)
	if (not os.execute(com .. ">/dev/null")) then
		io.stderr:write("[LBUILD] Error! Exiting.\n")
		os.exit(-1)
	end
end

function list_files(dir, func)
	local hand = io.popen("ls "..dir, "r")
	for line in hand:lines() do
		func(line)
	end
end

function rm(path)
	if (not os.execute("stat "..path..">/dev/null 2>&1")) then return end
	print("RM", path)
	exec("rm "..path)
end

function clean(dir)
	print("CLEAN", dir)
	list_files(dir, function(file)
		if (os.execute("[[ -d "..dir.."/"..file.." ]]")) then
			clean(dir.."/"..file)
		elseif (file:match("%.s?o")) then
			print("RM", file)
			rm(dir.."/"..file)
		end
	end)
end

function check_exec(path)
	local hand = io.popen("whereis -b "..path, "r")
	local res = hand:read("*a")
	local exec = res:match("(/.-)%s")
	print("EXEC", path, exec)
	if not exec then
		io.stderr:write(path.." not found, exiting...\n")
		os.exit(-1)
	end
	return exec
end

function check_dll(path)
	local hand = io.popen("whereis -b lib"..path, "r")
	local res = hand:read("*a")
	local exec = res:match("(/.-)%s")
	print("DLL", path, exec)
	if not exec then
		io.stderr:write(path.." not found, exiting...\n")
		os.exit(-1)
	end
	return exec
end

print("Checking Linux enviroment...")

--Check enviroment
local gcc = check_exec("gcc")
check_exec("lz4")
check_dll("lz4")
check_exec("xz")
check_exec("lzma")
check_dll("lzma")
check_exec("bzip2")
check_dll("bz")
check_exec("gzip")
check_dll("z")
check_exec("xxhsum")
check_dll("xxhash")
--Done checking enviroment

--Libs with executables
function make_extension(name, path, force)
	if (os.execute("stat src/"..path.."/.nobuild>/dev/null 2>&1") and not force) then
		return
	end
	print("MKEXT", name)
	local files = {}
	links = links or {}
	if (os.execute("stat src/"..path.."/links.txt>/dev/null 2>&1")) then
		local ln = io.open("src/"..path.."/links.txt", "r")
		for l in ln:lines() do
			if (l ~= "") then
				links[#links+1] = l
			end
		end
	end
	list_files("src/"..path, function(file)
		if (file:match("%.c$")) then
			local obj = file:gsub("%.c$", ".o")
			print("CC", path.."/"..obj)
			exec(gcc.." -g -fpic -c src/"..path.."/"..file.." -o src/"..path.."/"..obj.." -I./include")
			files[#files+1] = "src/"..path.."/"..obj
		end
	end)
	print("LINK", name..".so")
	exec(gcc.." -shared "..table.concat(files, " ")..(((#links > 0) and (" -l"..table.concat(links, " -l"))) or "").." -o extensions/"..name..".so")
end
--End of libs with executables

--clean
if (arg[1] == "clean") then
	print("Cleaning...")
	clean(".")
	--What else to delete
	rm("zxr")
	rm("zxr-extdump/zxr-extdump")
	--end of what else to delete
	os.exit(0)
end
--end of clean
print("Beginning compile...")

if (arg[1] == "mkext") then
	if (os.execute("[[ -d src/"..arg[2].." ]]")) then
		make_extension(arg[3], arg[2], true)
	else
		io.stderr:write("Not an extension.\n")
	end
	os.exit(0)
end

--build script

list_files("src/compression", function(file)
	if (os.execute("[[ -d src/compression/"..file.." ]]")) then
		make_extension("comp_"..file, "compression/"..file)
	end
end)

list_files("src/checksums", function(file)
	if (os.execute("[[ -d src/checksums/"..file.." ]]")) then
		make_extension("csum_"..file, "checksums/"..file)
	end
end)

print("CC", "zxr-extdump/main.c")
exec(gcc.." zxr-extdump/main.c -o zxr-extdump/zxr-extdump -ldl -Iinclude")

local srcfiles = {}
list_files("src", function(file)
	if (file:match("%.c$")) then
		local obj = file:gsub("%.c$", ".o")
		print("CC", obj)
		exec(gcc.." -g -c src/"..file.." -o src/"..obj.." -Iinclude")
		srcfiles[#srcfiles+1] = "src/"..obj
	end
end)

print("LINK", "zxr")
exec(gcc.." "..table.concat(srcfiles, " ").." -ldl -o zxr")

--make_extension("comp_none", "compression/none")