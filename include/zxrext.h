#ifndef __zxr_EXT
#define __zxr_EXT
#include <stdlib.h>

typedef struct {
	char * name;
	char id;
	char default_level;
	size_t (*compress)(char * in, char * out, size_t in_len);
	size_t (*decompress)(char * in, char * out, size_t in_len);
	void (*init)(char level, size_t buffer_size);
	void (*cleanup);
} zxr_comp;

typedef struct {
	char * name;
	char id;
	char size;
	char * (*get_sum)(char *,size_t, char *);
	char (*check)(char *,char *,size_t);
	void (*free)(char *);
} zxr_csum;

typedef struct {
	char * name;
	char * version;
	char * author;
	char * bugreport;
	char * website;
} zxr_extinfo;

#endif