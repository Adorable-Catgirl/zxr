#ifndef __CSUM_BASE
#define __CSUM_BASE
#include <stdint.h>
typedef struct {
	char size;
	char * (*get_sum)(char *,size_t);
	char (*check)(char *,char *,size_t);
} orf_csum;

#endif