#ifndef __CSUM_LIST
#define __CSUM_LIST

#include "checksum.h"

#include "xxh32.h"
#include "xxh64.h"
//#include "crc32.h"
//#include "crc8.h"

typedef struct {
	char * name;
	orf_csum info;
} orf_csum_info;

orf_csum_info orf_checksums[] = {
	{"xxh32", csum_xxh32},
	{"xxh64", csum_xxh64},
	//{"crc32", csum_crc32},
	//{"crc8", csum_crc8},
	{0}
};

#endif