#include "crc32.h"
#include <zlib.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * crc32_get_checksum(char * chunk, size_t length, char * buf) {
	uint32_t sum = crc32(crc32(0, NULL, 0), chunk, length);
	memcpy(buf, &sum, 4);
	return buf;
}

char crc32_validate(char * chunk, char * bsum, size_t length) {
	uint32_t sum = ((uint32_t *) bsum)[0];
	uint32_t csum = crc32(crc32(0, NULL, 0), chunk, length);
	return sum == csum;
}

void sum_free(char * c) {
	free(c);
}

zxr_csum crc32_csum = {
	"crc32",
	2,
	4,
	crc32_get_checksum,
	crc32_validate,
	sum_free
};

zxr_extinfo crc32_info = {
	"CRC32",
	"1.0",
	"Jane Roxanne",
	"janeroxanne0@gmail.com",
	"N/A"
};