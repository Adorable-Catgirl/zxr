#ifndef __crc32_CSUM
#define __crc32_CSUM

#include <zxrext.h>
#include <stdint.h>

extern zxr_csum crc32_csum;
extern zxr_extinfo crc32_info;

#endif