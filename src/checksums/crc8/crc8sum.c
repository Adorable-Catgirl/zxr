#include "crc8sum.h"
#include "crc8.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * crc8_get_checksum(char * chunk, size_t length, char * buf) {
	char sum = crc8(chunk, length);
	memcpy(buf, &sum, 1);
	return buf;
}

char crc8_validate(char * chunk, char * bsum, size_t length) {
	char sum = bsum[0];
	char csum = crc8(chunk, length);
	return sum == csum;
}

void sum_free(char * c) {
	free(c);
}

zxr_csum crc8_csum = {
	"crc8",
	3,
	1,
	crc8_get_checksum,
	crc8_validate,
	sum_free
};

zxr_extinfo crc8_info = {
	"CRC8",
	"1.0",
	"Jane Roxanne",
	"janeroxanne0@gmail.com",
	"N/A"
};