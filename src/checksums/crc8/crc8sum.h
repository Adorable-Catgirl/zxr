#ifndef __XXH32_CSUM
#define __XXH32_CSUM

#include <zxrext.h>
#include <stdint.h>

extern zxr_csum xxh32_csum;
extern zxr_extinfo xxh32_info;

#endif