#include "xxh32.h"
#include <xxhash.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * xxh32_get_checksum(char * chunk, size_t length, char * buf) {
	uint32_t sum = XXH32(chunk, length, 0);
	memcpy(buf, &sum, 4);
	return buf;
}

char xxh32_validate(char * chunk, char * bsum, size_t length) {
	uint32_t sum = ((uint32_t *) bsum)[0];
	uint32_t csum = XXH32(chunk, length, 0);
	return sum == csum;
}

void sum_free(char * c) {
	free(c);
}

zxr_csum xxh32_csum = {
	"xxh32",
	0,
	4,
	xxh32_get_checksum,
	xxh32_validate,
	sum_free
};

zxr_extinfo xxh32_info = {
	"XXHash32",
	"1.0",
	"Jane Roxanne",
	"janeroxanne0@gmail.com",
	"N/A"
};