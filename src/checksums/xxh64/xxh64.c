#include "xxh64.h"
#include <xxhash.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * xxh64_get_checksum(char * chunk, size_t length, char * buf) {
	uint32_t sum = XXH64(chunk, length, 0);
	memcpy(buf, &sum, 8);
	return buf;
}

char xxh64_validate(char * chunk, char * bsum, size_t length) {
	uint64_t sum = ((uint64_t *) bsum)[0];
	uint64_t csum = XXH64(chunk, length, 0);
	return sum == csum;
}

void sum_free(char * c) {
	free(c);
}

zxr_csum xxh64_csum = {
	"xxh64",
	1,
	4,
	xxh64_get_checksum,
	xxh64_validate,
	sum_free
};

zxr_extinfo xxh64_info = {
	"XXHash64",
	"1.0",
	"Jane Roxanne",
	"janeroxanne0@gmail.com",
	"N/A"
};