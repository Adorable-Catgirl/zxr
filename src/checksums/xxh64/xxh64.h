#ifndef __XXH64_CSUM
#define __XXH64_CSUM

#include <zxrext.h>
#include <stdint.h>

extern zxr_csum xxh64_csum;
extern zxr_extinfo xxh64_info;

#endif