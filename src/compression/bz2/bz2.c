#include "./bz2.h"
#include <bzlib.h>

char level;
uint32_t blksize;

size_t bz2_compress_block(char * in, char * out, size_t in_len) {
	uint32_t len = (uint32_t) in_len*2;
	if (BZ2_bzBuffToBuffCompress(out, &len, in, in_len, level, 0, 0) != BZ_OK) {
		fprintf(stderr, "\033[31mE\033[0m: BZip2 compression error.\n");
		abort();
	}
	return len;
}

size_t bz2_decompress_block(char * in, char * out, size_t in_len) {
	if (BZ2_bzBuffToBuffDecompress(out, &blksize, in, in_len, 0, 0) != BZ_OK) {
		fprintf(stderr, "\033[31mE\033[0m: BZip2 decompression error.\n");
		abort();
	}
	return blksize;
}

void bz2_init(char lv, size_t bz) {
	level = lv;
	blksize = bz;
}

void bz2_cleanup() {

}

zxr_comp bz2_comp = {
	"bz2",
	3,
	4,
	bz2_compress_block,
	bz2_decompress_block,
	bz2_init,
	bz2_cleanup
};

zxr_extinfo bz2_info = {
	"BZip2",
	"1.0",
	"Jane Roxanne",
	"janeroxanne0@gmail.com",
	"N/A"
};