#ifndef __BZ2_CSUM
#define __BZ2_CSUM

#include <zxrext.h>
#include <stdint.h>

extern zxr_csum bz2_csum;
extern zxr_extinfo bz2_info;

#endif