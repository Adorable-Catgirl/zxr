/* 
	Copyright 2019 Rose Engine Dev Group

	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	You may obtain a copy of the License at

	    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.

	RE_Compat.h

	Part of the Rose Engine SDK. This header enables the use of many low-level Rose
	Engine libraries in other projects, such as OpenHM or OpenTH. It's a single
	header library, and, as such, no other C file are needed.

	This uses SSE/SSE2.
	
	Original file can be found at:
	https://gitlab.com/renginedev/rose-engine-sdk/blob/master/include/RE_Compat.h
*/

#ifndef __RECOMPAT
#define __RECOMPAT

#include <stdlib.h>
#include <stdint.h>
#include <immintrin.h>
#include <string.h>

// Shorthand types. Required for Rose Engine compatibility.
// 8 bits
typedef uint8_t u8;
typedef int8_t s8;
typedef volatile u8 vu8;
typedef volatile s8 vs8;

// 16 bits
typedef uint16_t u16;
typedef int16_t s16;
typedef volatile u16 vu16;
typedef volatile s16 vs16;

// 32 bits
typedef uint32_t u32;
typedef int32_t s32;
typedef volatile u32 vu32;
typedef volatile s32 vs32;

// 64 bits
typedef uint64_t u64;
typedef int64_t s64;
typedef volatile u64 vu64;
typedef volatile s64 vs64;

// Floating point decimals
typedef float f32;
typedef volatile f32 vf32;
typedef double f64;
typedef volatile f64 vf64;

#ifdef __RE_F32
typedef f32 RE_Float;
typedef __128 __rmx;
#define RMX_MEMBERS 4
#define RMX_ADD(a, b) _mm_add_ps(a, b)
#define RMX_SUB(a, b) _mm_sub_ps(a, b)
#define RMX_MUL(a, b) _mm_mul_ps(a, b)
#define RMX_DIV(a, b) _mm_div_ps(a, b)
#define RMX_MAKE(x, y) _mm_set_ps(x, y, 0, 0)
#else
typedef f64 RE_Float;
typedef __m128d __rmx;
#define RMX_MEMBERS 2
#define RMX_ADD(a, b) _mm_add_pd(a, b)
#define RMX_SUB(a, b) _mm_sub_pd(a, b)
#define RMX_MUL(a, b) _mm_mul_pd(a, b)
#define RMX_DIV(a, b) _mm_div_pd(a, b)
#define RMX_MAKE(x, y) _mm_set_pd(x, y)
#endif
typedef RE_Float rfp;
typedef volatile rfp vrfp;

typedef union {
	 __rmx __mat;
	 rfp __member[RMX_MEMBERS];
} RE_Position;

inline RE_Position repos_new(rfp x, rfp y) {
	RE_Position rtn;
	rtn.__mat = RMX_MAKE(x, y);
	return rtn;
}

inline RE_Position repos_add(RE_Position pos, rfp x, rfp y) {
	RE_Position rtn;
	rtn.__mat = RMX_ADD(pos.__mat, RMX_MAKE(x, y));
	return rtn;
}

inline RE_Position repos_sub(RE_Position pos, rfp x, rfp y) {
	RE_Position rtn;
	rtn.__mat = RMX_SUB(pos.__mat, RMX_MAKE(x, y));
	return rtn;
}

inline RE_Position repos_mul(RE_Position pos, rfp x, rfp y) {
	RE_Position rtn;
	rtn.__mat = RMX_MUL(pos.__mat, RMX_MAKE(x, y));
	return rtn;
}

inline RE_Position repos_div(RE_Position pos, rfp x, rfp y) {
	RE_Position rtn;
	rtn.__mat = RMX_DIV(pos.__mat, RMX_MAKE(x, y));
	return rtn;
}

inline RE_Position repos_addp(RE_Position pos, RE_Position pos2) {
	RE_Position rtn;
	rtn.__mat = RMX_ADD(pos.__mat, pos2.__mat);
	return rtn;
}

inline RE_Position repos_subp(RE_Position pos, RE_Position pos2) {
	RE_Position rtn;
	rtn.__mat = RMX_SUB(pos.__mat, pos2.__mat);
	return rtn;
}

inline RE_Position repos_mulp(RE_Position pos, RE_Position pos2) {
	RE_Position rtn;
	rtn.__mat = RMX_MUL(pos.__mat, pos2.__mat);
	return rtn;
}

inline RE_Position repos_divp(RE_Position pos, RE_Position pos2) {
	RE_Position rtn;
	rtn.__mat = RMX_DIV(pos.__mat, pos2.__mat);
	return rtn;
}

inline rfp repos_getx(RE_Position pos) {
	return pos.__member[0];
}

inline rfp repos_gety(RE_Position pos) {
	return pos.__member[1];
}

typedef union {
	u8 bytes[3];
	struct {
		u8 r;
		u8 g;
		u8 b;
	} colors;
} RE_RGBColor;

typedef union {
	u8 bytes[4];
	struct {
		u8 r;
		u8 g;
		u8 b;
		u8 a;
	} colors;
} RE_RGBAColor;

#endif