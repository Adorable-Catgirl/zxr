#include "RE_Compression.h"
#include "stdio.h"

lz4_compress_stream_t * lz4_stream_init(u8 level, u32 buffer_size) {
	if (level == 0) {
		level = 2;
	}
	if (buffer_size == 0) {
		buffer_size = 65536;
	}
	if (buffer_size > 1024) {
		buffer_size = 1024;
	}
	lz4_compress_stream_t * p = malloc(sizeof(lz4_compress_stream_t));
	LZ4_resetStreamHC(&p->handle, 2);
	p->level = 2;
	p->buffer_pos = 0;
	p->buffer = malloc(65536);
	return p;
}

u8 _ring_policy(u32 buffer_size, u32 buffer_pos, u32 data_size) {
	if (data_size > buffer_size || data_size > LZ4_DICTSIZE || data_size + LZ4_DICTSIZE > buffer_pos)
		return RING_POLICY_EXTERNAL;
	if (buffer_pos + data_size <= buffer_size)
		return RING_POLICY_APPEND;
	return RING_POLICY_RESET;
}

u32 lz4_shc_reset(lz4_compress_stream_t *l, char * in, u64 in_len) {
	if (in != NULL && in_len > 0) {
		u32 limit_len = LZ4_DICTSIZE;
		if (limit_len > l->buffer_size) limit_len = l->buffer_size;
		if (in_len > limit_len) {
			in = in+in_len-limit_len;
			in_len = limit_len;
		}
		memcpy(l->buffer, in, in_len);
		l->buffer_pos = LZ4_loadDictHC(&l->handle, l->buffer, in_len);
	} else {
		LZ4_resetStreamHC(&l->handle, l->level);
		l->buffer_pos = 0;
	}

	return l->buffer_pos;
}

char * lz4_shc_compress(lz4_compress_stream_t *cs, char * in, u32 in_len, size_t *out_len) {
	size_t bound = LZ4_compressBound(in_len);
	u8 policy = _ring_policy(cs->buffer_size, cs->buffer_pos, in_len);
	u32 r;

	char * out = malloc(bound);

	if (policy == RING_POLICY_APPEND || policy == RING_POLICY_RESET) {
		char * ring;
		if (policy == RING_POLICY_APPEND) {
			ring = cs->buffer + cs->buffer_pos;
		} else {
			ring = cs->buffer;
			cs->buffer_pos = in_len;
		}
		memcpy(ring, in, in_len);
		r = LZ4_compress_HC_continue(&cs->handle, ring, out, in_len, bound);
		if (r == 0) {
			printf("Compression failed.\n");
			abort();
			return NULL;
		}
	} else {
		r = LZ4_compress_HC_continue(&cs->handle, in, out, in_len, bound);
		if (r == 0) {
			printf("Compression failed.\n");
			abort();
			return NULL;
		}
		cs->buffer_pos = LZ4_saveDictHC(&cs->handle, cs->buffer, cs->buffer_size);
	}

	*out_len = r;

	return out;
}

lz4_decompress_stream_t * lz4_dstream_init(u32 buffer_size) {
	lz4_decompress_stream_t * p = malloc(sizeof(lz4_decompress_stream_t));
	LZ4_setStreamDecode(&p->handle, NULL, 0);
	p->buffer_size = buffer_size;
	p->buffer_pos = 0;
	p->buffer = malloc(buffer_size);
	return p;
}

void _lz4_ds_save_dict(lz4_decompress_stream_t *ds, char * dict, u32 dict_size) {
	u32 limit_len = LZ4_DICTSIZE;
	if (limit_len > ds->buffer_size) limit_len = ds->buffer_size;
	if (dict_size > limit_len) {
		dict += dict_size - limit_len;
		dict_size = limit_len;
	}

	memmove(ds->buffer, dict, dict_size);
	LZ4_setStreamDecode(&ds->handle, ds->buffer, dict_size);

	ds->buffer_pos = dict_size;
}

u32 lz4_dec_reset(lz4_decompress_stream_t * ds, char * in, u32 in_len) {
	if (in != NULL && in_len > 0) {
		u32 limit_len = LZ4_DICTSIZE;
		if (limit_len > ds->buffer_size) limit_len = ds->buffer_size;
		if (in_len > limit_len) {
			in = in + in_len - limit_len;
			in_len = limit_len;
		}
		memcpy(ds->buffer, in, in_len);
		ds->buffer_pos = LZ4_setStreamDecode(&ds->handle, ds->buffer, in_len);
	} else {
		LZ4_setStreamDecode(&ds->handle, NULL, 0);
		ds->buffer_pos = 0;
	}
	return ds->buffer_pos;
}

char * lz4_decompress_safe(lz4_decompress_stream_t * ds, char * in, u32 in_len, size_t * out_len) {
	u8 policy = _ring_policy(ds->buffer_size, ds->buffer_pos, *out_len);
	s32 r;

	if (policy == RING_POLICY_APPEND || policy == RING_POLICY_RESET) {
		char * ring;
		size_t new_pos;
		if (policy == RING_POLICY_APPEND) {
			ring = ds->buffer + ds->buffer_pos;
			new_pos = ds->buffer_pos + *out_len;
		} else {
			ring = ds->buffer;
			new_pos = *out_len;
		}

		r = LZ4_decompress_safe_continue(&ds->handle, in, ring, in_len, *out_len);
		if (r < 0) return NULL;
		ds->buffer_pos = new_pos;
		*out_len = r;
		char * rbuf = malloc(r);
		memcpy(ring, rbuf, r);
		return rbuf;
	} else {
		char * out = malloc(*out_len);
		r = LZ4_decompress_safe_continue(&ds->handle, in, out, in_len, *out_len);
		*out_len = r;
		if (r<0) {
			free(out);
			return NULL;
		}
		_lz4_ds_save_dict(ds, out, r);
		return out;
	}
}

char * lz4_decompress_fast(lz4_decompress_stream_t * ds, char * in, u32 in_len, size_t * out_len) {
	u8 policy = _ring_policy(ds->buffer_size, ds->buffer_pos, *out_len);
	s32 r;

	if (policy == RING_POLICY_APPEND || policy == RING_POLICY_RESET) {
		char * ring;
		size_t new_pos;
		if (policy == RING_POLICY_APPEND) {
			ring = ds->buffer + ds->buffer_pos;
			new_pos = ds->buffer_pos + *out_len;
		} else {
			ring = ds->buffer;
			new_pos = *out_len;
		}

		r = LZ4_decompress_fast_continue(&ds->handle, in, ring, *out_len);
		if (r < 0) return NULL;
		ds->buffer_pos = new_pos;
		*out_len = r;
		char * rbuf = malloc(r);
		memcpy(ring, rbuf, r);
		return rbuf;
	} else {
		char * out = malloc(*out_len);
		r = LZ4_decompress_fast_continue(&ds->handle, in, out, *out_len);
		*out_len = r;
		if (r<0) {
			free(out);
			return NULL;
		}
		_lz4_ds_save_dict(ds, out, r);
		return out;
	}
}

u8 lz4_shc_free(lz4_compress_stream_t *cs) {
	free(cs->buffer);
	free(cs);
	return 1;
}

u8 lz4_dec_free(lz4_decompress_stream_t *ds) {
	free(ds->buffer);
	free(ds);
	return 1;
}