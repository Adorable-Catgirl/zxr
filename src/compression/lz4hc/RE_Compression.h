#ifndef __RE_Z
#define __RE_Z
#include "RE_Compat.h"
#include <lz4.h>
#include <lz4hc.h>
#define LZ4_DICTSIZE 65536
#define RING_POLICY_APPEND 0
#define RING_POLICY_RESET 1
#define RING_POLICY_EXTERNAL 2

typedef struct {
	LZ4_streamHC_t handle;
	u8 level;
	u32 buffer_size;
	u32 buffer_pos;
	char *buffer;
} lz4_compress_stream_t;

typedef struct {
	LZ4_streamDecode_t handle;
	u32 buffer_size;
	u32 buffer_pos;
	char *buffer;
} lz4_decompress_stream_t;

typedef lz4_compress_stream_t lz4str_t;
typedef lz4_decompress_stream_t lz4dstr_t;

lz4_compress_stream_t * lz4_stream_init(u8 level, u32 buffer_size);
u32 lz4_shc_reset(lz4_compress_stream_t *cs, char * in, u64 in_len);
char * lz4_shc_compress(lz4_compress_stream_t *cs, char * in, u32 in_len, size_t *out_len);
u8 lz4_shc_free(lz4_compress_stream_t * cs);
lz4_decompress_stream_t * lz4_dstream_init(u32 buffer_size);
u32 lz4_dec_reset(lz4_decompress_stream_t *ds, char * in, u32 in_len);
char * lz4_decompress_safe(lz4_decompress_stream_t * ds, char * in, u32 in_len, size_t *out_len);
char * lz4_decompress_fast(lz4_decompress_stream_t * ds, char * in, u32 in_len, size_t *out_len);
u8 lz4_dec_free(lz4_decompress_stream_t * ds);

#endif