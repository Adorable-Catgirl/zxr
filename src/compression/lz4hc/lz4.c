#include "lz4.h"

lz4str_t * comp = NULL;
lz4dstr_t * decp = NULL;

char level = 0;
size_t buffer_size = 0;

size_t lz4_compress_block(char * in, char * out, size_t in_len) {
	if (comp == NULL) {
		comp = lz4_stream_init(level, buffer_size);
	}
	size_t len = 0;
	char * dat = lz4_shc_compress(comp, in, in_len, &len);
	memcpy(out, dat, len);
	free(dat);
	return len;
}

size_t lz4_decompress_block(char * in, char * out, size_t in_len) {
	if (comp == NULL) {
		decp = lz4_dstream_init(buffer_size);
	}
	size_t len = buffer_size;
	char * dat = lz4_decompress_safe(decp, in, in_len, &len);
	memcpy(out, dat, buffer_size);
	free(dat);
	return buffer_size;
}

void lz4_init(char cl, size_t bz) {
	level = cl;
	buffer_size = bz;
}

void lz4_cleanup() {

}

zxr_comp lz4hc_comp = {
	"lz4hc",
	1,
	9,
	lz4_compress_block,
	lz4_decompress_block,
	lz4_init,
	lz4_cleanup
};

zxr_extinfo lz4hc_info = {
	"LZ4HC",
	"1.0",
	"Jane Roxanne",
	"janeroxanne0@gmail.com",
	"N/A"
};