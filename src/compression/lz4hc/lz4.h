#ifndef __LZ4_COMP
#define __LZ4_COMP

#include "RE_Compression.h"
#include "RE_Compat.h"
#include <string.h>
#include <zxrext.h>

extern zxr_comp lz4hc_comp;
extern zxr_extinfo lz4hc_info;

#endif