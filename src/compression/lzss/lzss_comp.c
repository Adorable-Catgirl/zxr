#include "./lzss_comp.h"
#include "./lzss.h"
#include <string.h>

size_t lzss_compress_block(char * in, char * out, size_t in_len) {
	
	return in_len;
}

size_t lzss_decompress_block(char * in, char * out, size_t in_len) {
	memcpy(in, out, in_len);
	return in_len;
}

void lzss_init(char lv, size_t bz) {

}

void lzss_cleanup() {

}

zxr_comp lzss_comp = {
	"lzss",
	2,
	0,
	lzss_compress_block,
	lzss_decompress_block,
	lzss_init,
	lzss_cleanup
};

zxr_extinfo lzss_info = {
	"LZSS",
	"1.0",
	"Jane Roxanne",
	"janeroxanne0@gmail.com",
	"N/A"
};