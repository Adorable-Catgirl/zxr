#ifndef __LZSS_CSUM
#define __LZSS_CSUM

#include <zxrext.h>
#include <stdint.h>

extern zxr_csum lzss_csum;
extern zxr_extinfo lzss_info;

#endif