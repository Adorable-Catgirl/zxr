#include "./none.h"
#include <string.h>

size_t none_compress_block(char * in, char * out, size_t in_len) {
	memcpy(out, in, in_len);
	return in_len;
}

size_t none_decompress_block(char * in, char * out, size_t in_len) {
	memcpy(out, in, in_len);
	return in_len;
}

void none_init(char lv, size_t bz) {

}

void none_cleanup() {

}

zxr_comp none_comp = {
	"none",
	0,
	0,
	none_compress_block,
	none_decompress_block,
	none_init,
	none_cleanup
};

zxr_extinfo none_info = {
	"No",
	"1.0",
	"Jane Roxanne",
	"janeroxanne0@gmail.com",
	"N/A"
};