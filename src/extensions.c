#include "extensions.h"
#include <dlfcn.h>
#include <dirent.h>
#include <zxrext.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

typedef struct {
	char * name;
	char * path;
	char * basename;
	char * type;
	zxr_extinfo * info;
	char * libsym;
	void * lib;
} zxrext;

zxrext * extensions = NULL;

uint8_t isso(struct dirent *E) {
	uint8_t namelen = strlen(E->d_name);
	return !strcmp(".so", E->d_name+namelen-3);
}

uint16_t zxrext_count() {
	uint16_t i = 0;
	while (extensions[i].name != NULL) ++i;
	return i;
}

void zxrext_enumerate(char * path) {
	if (extensions == NULL) {
		extensions = malloc(sizeof(zxrext));
	}
	DIR * extdir = opendir(path);
	if (extdir == NULL) {
		return;
	}
	struct dirent *ent = readdir(extdir);
	while (ent != NULL) {
		if (isso(ent)) {
			char * opath = malloc(strlen(path)+strlen(ent->d_name)+2);
			char * objname = malloc(strlen(ent->d_name)+1);
			strcpy(objname, ent->d_name);
			sprintf(opath, "%s/%s", path, ent->d_name);
			void *lib = dlopen(opath, RTLD_NOW);
			char * libname = NULL;
			// find underscore
			for (char i = 0; i < strlen(objname); ++i) {
				if (objname[i] == '_') {
					libname = objname+i+1;
					break;
				}
			}
			char * name = malloc(strlen(libname)-2);
			memcpy(name, libname, strlen(libname)-3);
			char * sym = malloc(strlen(name)+6);
			sprintf(sym, "%s_info", name);
			char * libsym = malloc(5);
			memcpy(libsym, objname, 4);
			char * libt = malloc(strlen(sym)+6);
			sprintf(libt, "%s_%s", name, libsym);
			zxr_extinfo * info = dlsym(lib, sym);
			uint16_t extcount = zxrext_count();
			zxrext * extnew = malloc((extcount+2)*sizeof(zxrext));
			free(sym);
			if (extcount > 0) {
				memcpy(extnew, extensions, extcount*sizeof(zxrext));
			}
			extnew[extcount].name = objname;
			extnew[extcount].basename = name;
			extnew[extcount].path = opath;
			extnew[extcount].type = libsym;
			extnew[extcount].libsym = libt;
			extnew[extcount].info = info;
			extnew[extcount].lib = lib;
			free(extensions);
			extensions = extnew;
		}
		ent = readdir(extdir);
	}
	closedir(extdir);
}

void * zxrext_getext(uint16_t ext) {
	return dlsym(extensions[ext].lib, extensions[ext].libsym);
}

uint16_t zxrext_getextid(char * ext) {
	uint16_t extc = zxrext_count();
	for (uint16_t i=0; i<extc;++i) {
		if (strcmp(extensions[i].libsym, ext) == 0) {
			return i;
		}
	}
	return 65535;
}

char * zxrext_getname(uint16_t ext) {
	return extensions[ext].libsym;
}

zxr_extinfo * zxrext_getinfo(uint16_t ext) {
	return extensions[ext].info;
}

char zxrext_gettype(uint16_t ext) {
	if (strcmp("comp", extensions[ext].type) == 0)
		return ZXR_EXT_COMPRESSOR;
	else if (strcmp("csum", extensions[ext].type) == 0)
		return ZXR_EXT_CHECKSUM;
	else
		return ZXR_EXT_OTHER;
}

void zxrext_cleanup() {
	uint16_t extc = zxrext_count();
	for (uint16_t i=0; i<extc;++i) {
		free(extensions[i].libsym);
		free(extensions[i].path);
		free(extensions[i].basename);
		free(extensions[i].type);
		free(extensions[i].name);
		dlclose(extensions[i].lib);
	}
	free(extensions);
}