#ifndef __zxr_EXTM
#define __zxr_EXTM

#include <zxrext.h>
#include <stdint.h>
#define ZXR_EXT_COMPRESSOR 0
#define ZXR_EXT_CHECKSUM 1
#define ZXR_EXT_OTHER 2

uint16_t zxrext_count();
void zxrext_enumerate(char * path);
void * zxrext_getext(uint16_t id);
uint16_t zxrext_getextid(char * ext);
zxr_extinfo * zxrext_getinfo(uint16_t id);
char * zxrext_getname(uint16_t ext);
char zxrext_gettype(uint16_t ext);
void zxrext_cleanup();
#endif