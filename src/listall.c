#include "listall.h"
#include <string.h>
#include <stdint.h>

typedef struct {
	size_t pos;
	char type;
	uint64_t size;
	char *data;
} zxr_entry;

uint64_t read_vli(zxr_hand *W) {
	char lc;
	char pos = 0;
	uint64_t amt = 0;
	do {
		zxr_read(W, &lc, 1);
		amt |= (lc & 0x7F) << (pos*7);
		pos++;
	} while (lc & 0x80);
	return amt;
}

uint64_t read_vli_s(char *str, char *len) {
	char lc;
	*len = 0;
	uint64_t amt = 0;
	do {
		lc = str[*len];
		amt |= (lc & 0x7F) << (*len*7);
		*len++;
	} while (lc & 0x80);
	return amt;
}

zxr_entry * read_entry(zxr_hand *W) {
	zxr_entry *ent = malloc(sizeof(zxr_entry));
	ent->pos = zxr_pos(W);
	ent->size = read_vli(W);
	zxr_read(W, &ent->type, 1);
	ent->data = malloc(ent->size);
	zxr_read(W, ent->data, ent->size);
	return ent;
}
typedef struct {
	char *name;
	uint64_t id;
	uint64_t parent;
	zxr_entry * ent;
} zxr_obj;

zxr_obj * read_object(zxr_entry *W) {
	if (W->type != 0 && W->type != 1) return NULL;
	zxr_obj *obj = malloc(sizeof(zxr_obj));
	uint16_t offset = 0;
	char vlilen = 0;
	uint64_t namelen = read_vli_s(W->data, &vlilen);
	offset += vlilen;
	obj->name = malloc(namelen+1);
	strcpy(obj->name, W->data+offset);
	offset += namelen;
	obj->id = read_vli_s(W->data+offset, &vlilen);
	offset += vlilen;
	obj->parent = read_vli_s(W->data+offset, &vlilen);
	obj->ent = W;
	return obj;
}

zxr_obj * get_entry(zxr_hand * W, uint64_t id) {
	if (id == 0) return NULL;
	zxr_seek(W, 0);
	zxr_obj *obj = NULL;
	do {
		zxr_entry *ent = read_entry(W);
		while (ent->type != 1 && ent->type != 0) {
			free(ent->data);
			free(ent);
			ent = read_entry(W);
			if (ent->type == 0x7f) {
				free(ent->data);
				free(ent);
				return NULL;
			}
		}
		if (obj != NULL && obj->id == id)
			return obj;
		else {
			free(obj->ent->data);
			free(obj->ent);
			free(obj->name);
			free(obj);
		}
	} while (obj != NULL);
	return obj;
}

void freeobj(zxr_obj *obj) {
	free(obj->ent->data);
	free(obj->ent);
	free(obj->name);
	free(obj);
}

char * getpath(zxr_hand *W, zxr_obj *O) {
	zxr_obj *cur_obj = O;
	char *path = malloc(strlen(O->name)+2);
	sprintf(path, "/%s", O->name);
	do {
		uint64_t pid = cur_obj->parent;
		if (cur_obj != O)
			freeobj(cur_obj);
		cur_obj = get_entry(W, pid);
		if (cur_obj) {
			char *tpath = malloc(strlen(path)+strlen(cur_obj->name)+2);
			sprintf(tpath, "/%s%s", cur_obj->name, path);
			free(path);
			path = tpath;
		}
	} while (cur_obj != NULL);
	return path;
}

void list_files(zxr_hand *W) {
	zxr_checktypes(W);
	//Get all objects.
	zxr_obj **objects = malloc(sizeof(zxr_obj));
	zxr_entry *lastent = read_entry(W);
	while (lastent->type != 0x7F) {
		if (lastent->type < 2) {
			zxr_obj *obj = read_object(lastent);
			uint64_t count = 0;
			while (objects[count] != NULL) ++count;
			zxr_obj **objl = malloc(sizeof(zxr_obj)*(count+2));
			if (count != 0) {
				memcpy(objl, objects, count*sizeof(void *));
			}
			objl[count] = obj;
			free(objects);
			objects = objl;
		}
	}
	uint64_t index = 0;
	while (objects[index] != NULL) {
		char * path = getpath(W, objects[index]);
		printf(".%s", path);
		free(path);
		free(objects[index]);
		index++;
	}
	free(objects);
}