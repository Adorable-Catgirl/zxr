#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <argp.h>
#include <string.h>
#include <inttypes.h>
#include <unistd.h>
#include "extensions.h"
#include "entries.h"
#include "zxrio.h"
#include "listall.h"
#include "makearchive.h"
#define META_STANDARD 1
#define META_EXTENDED 2
#define META_WINDOWS 4

// argp things

const char *zxr_version = "zxr 0.1";

const char *bug_addr = "<janeroxanne0@gmail.com>";

struct arguments {
	char mode;
	char *comp;
	char *csum;
	char *input;
	char *output;
	char meta;
	size_t chunk_size;
	char *link_from;
	linkt *links;
	char level;
	char verbose;
};

static struct argp_option options[] = {
	{"verbose",			'v',	0,			0,					 "Verbose output"},
	{"make",			'm',	"FILE",		0,					 "Makes an zxr archive"},
	{"extract",			'x',	"DIR",		0,					 "Extracts an zxr archive"},
	{"list",			'l',	0,			0,					 "Lists all files in an archive"},
	{"listdir", 		's',	"DIR",		0,					 "Lists all files in a directory of the archive"},
	{"compression", 	'z',	"METHOD",	OPTION_ARG_OPTIONAL, "Sets the compression method. (If not specified, none. If specified with no argument, LZSS)"},
	{"checksum", 		6,		"CHECKSUM",	0,					 "Sets the checksum method. (Default: xxh32)"},
	{"level",			'L',	"LEVEL",	0,					 "Sets the compression level (Default and max vary from compressor to compressor)"},
	{"strip-meta", 		1,		0,			0,					 "Does not write metadata."},
	{"extended-meta",	2,		0,			0,					 "Writes extended metadata. (Entry 0x06)"},
	{"windows-meta",	'w',	0,			0,					 "Writes Windows metadata."},
	{"link-from",		3,		"PATH",		0,					 "First part of a link"},
	{"link-to",			4,		"PATH",		0,					 "Second part of a link"},
	{"chunk-size",		5,		"SIZE",		0,					 "Sets chunk size, in bytes. (Default: 65536 bytes)"},
	{"extensions",		7,		0,			0,					 "Lists all extensions."},
	{0}
};

static size_t parse_readable(char * s) {
	char *endp = s;
	int sh;
	errno = 0;
	uintmax_t x = strtoumax(s, &endp, 10);
	if (errno || endp == s) return 0;
	switch(*endp) {
	case 'k': sh=10; break;
	case 'M': sh=20; break;
	case 'G': sh=30; break;
	case 0: sh=0; break;
	default: return 0;
	}
	if (x > SIZE_MAX>>sh) return 0;
	x <<= sh;
	return x;
}

static error_t parse_opt(int key, char *arg, struct argp_state *state) {
	struct arguments *arguments = state->input;
	switch (key) {
		case 7:
			if (arguments->mode)
				argp_usage(state);
			arguments->mode = 5;
			break;
		case 'm':
			if (arguments->mode)
				argp_usage(state);
			arguments->mode = 1;
			arguments->output = arg;
			break;
		case 'x':
			if (arguments->mode)
				argp_usage(state);
			arguments->mode = 2;
			arguments->output = arg;
			break;
		case 'l':
			if (arguments->mode)
				argp_usage(state);
			arguments->mode = 3;
			break;
		case 's':
			if (arguments->mode)
				argp_usage(state);
			arguments->mode = 4;
			arguments->output = arg;
			break;
		case 'z':
			if (arg == NULL) {
				arguments->comp = "lzss";
			} else {
				arguments->comp = arg;
			}
			break;
		case 6:
			arguments->csum = arg;
			break;
		case 1:
			arguments->meta = 0;
			break;
		case 2:
			if (arguments->meta) {
				arguments->meta |= META_EXTENDED;
			}
			break;
		case 'w':
			if (arguments->meta) {
				arguments->meta |= META_WINDOWS;
			}
			break;
		case 3:
			arguments->link_from = arg;
			break;
		case 4:
			;
			char i = 0;
			while (arguments->links != NULL && arguments->links[i].from != NULL)
				i++;
			linkt *links = malloc(sizeof(linkt)*(i+1));
			if (i) {
				memcpy(links, arguments->links, sizeof(linkt)*i);
				free(arguments->links);
			}
			links[i].from = arguments->link_from;
			links[i].to = arg;
			arguments->links = links;
			break;
		case 5:
			arguments->chunk_size = parse_readable(arg);
			break;
		case 'L':
			arguments->level = atoi(arg);
			break;
		case 'v':
			arguments->verbose = 1;
			break;
		case ARGP_KEY_ARG:
			if (state->arg_num > 1)
				argp_usage(state);
			arguments->input = arg;
			break;
		case ARGP_KEY_END:
			if (state->arg_num != 1 && arguments->mode != 5)
				argp_usage(state);
			break;
		default:
			return ARGP_ERR_UNKNOWN;
			break;
	}
	return 0;
}

static char * _ttn(char x) {
	switch(x) {
		case 0:
			return "Compressor";
		case 1:
			return "Checksum";
		default:
			return "Other";
	}
}

// TODO: Fill in this shit

static struct argp argp = {options, parse_opt, "INPUT", "zxr -- the ZXR archive tool."};

// main method, duh
int main(int argc, char *argv[]) {
	struct arguments arguments = {
		0,
		"none",
		"xxh32",
		NULL,
		NULL,
		0,
		65536,
		NULL,
		NULL,
		0,
		0
	};
	argp_parse(&argp, argc, argv, 0, 0, &arguments);
	zxrext_enumerate("extensions");
	char *comp = malloc(strlen(arguments.comp)+6);
	sprintf(comp, "%s_comp", arguments.comp);
	if (zxrext_getextid(comp) == 65535) {
		fprintf(stderr, "\033[31mE\033[0m: Compressor %s not found.\n", arguments.comp);
		return 1;
	}
	char *csum = malloc(strlen(arguments.csum)+6);
	sprintf(csum, "%s_csum", arguments.csum);
	if (zxrext_getextid(csum) == 65535) {
		fprintf(stderr, "\033[31mE\033[0m: Checksum %s not found.\n", arguments.csum);
		return 1;
	}
	if (arguments.mode == 5) {
		uint16_t extcount = zxrext_count();
		printf("Extensions: %d\n\n", extcount);
		for (uint16_t i=0; i<extcount; ++i) {
			zxr_extinfo *info = zxrext_getinfo(i);
			char type = zxrext_gettype(i);
			printf("%s\nType: %s\nName: %s\nVersion: %s\nAuthor: %s\nAuthor email: %s\nAuthor website: %s\n", 
				zxrext_getname(i), _ttn(type), info->name, info->version, info->author, 
				info->bugreport, info->website);
			switch(type) {
				case 0:
					;
					zxr_comp *zcomp = zxrext_getext(i);
					printf("ID: %s\nUUID: %d\nDefault level: %d\n", zcomp->name, zcomp->id, zcomp->default_level);
					break;
				case 1:
					;
					zxr_csum *zcsum = zxrext_getext(i);
					printf("ID: %s\nUUID: %d\nChecksum size: %d bytes\n", zcsum->name, zcsum->id, zcsum->size);
					break;
			}
			printf("\n");
		}
	}
	if (arguments.mode == 1) {
		zxr_hand hand;
		hand.chunk_size = arguments.chunk_size;
		hand.pos = 0;
		hand.last_chunk = 0;
		hand.compressor = zxrext_getext(zxrext_getextid(comp));
		hand.checksum = zxrext_getext(zxrext_getextid(csum));
		hand.buffer = malloc(arguments.chunk_size*2);
		if (strcmp(arguments.output, "-") == 0) {
			if (isatty(STDOUT_FILENO)) {
				fprintf(stderr, "Refusing to output to a terminal.\n");
				return 1;
			}
			hand.handle = stdout;
		} else
			hand.handle = fopen(arguments.output, "wb");
		hand.verbose = arguments.verbose;
		if (arguments.level == 0)
			arguments.level = hand.compressor->default_level;
		hand.level = arguments.level;
		make_zxr(arguments.input, &hand);
		fclose(hand.handle);
		free(hand.buffer);
	}
	if (arguments.mode == 3) {
		zxr_hand hand;
		if (strcmp(arguments.input, "-") == 0) {
			hand.handle = stdin;
		} else {
			hand.handle = fopen(arguments.input, "rb");
		}
		list_files(&hand);
	}
	zxrext_cleanup();
	free(csum);
	free(comp);
	return 0;
}