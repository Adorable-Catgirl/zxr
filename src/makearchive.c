#include "makearchive.h"

#include <dirent.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "utils.h"

typedef struct {
	char *path;
	char *name;
	char type;
	unsigned long long id, parent, size, offset;
	short mode;
	unsigned long long mtime, ctime, atime;
} zxrent;

zxrent *entries;
size_t ents = 0;

size_t offset = 0;
size_t id = 1;
void write_entry(zxr_hand *W, zxrent *ent) {
	uint16_t namelen = strlen(ent->name);
	char nl_l, id_l, parent_l, size_l, offset_l, mtime_l, ctime_l, atime_l, vli1_l, vli2_l;
	char *nl = tovli(namelen, &nl_l);
	char *id = tovli(ent->id, &id_l);
	char *parent = tovli(ent->parent, &parent_l);
	char *size = NULL;
	char *offset = NULL;
	if (ent->type == 1) {
		size = tovli(ent->size, &size_l);
		offset = tovli(ent->offset, &offset_l);
	} else {
		size_l = 0;
		offset_l = 0;
	}
	char *mtime = tovli(ent->mtime, &mtime_l);
	char *ctime = tovli(ent->ctime, &ctime_l);
	char *atime = tovli(ent->atime, &atime_l);
	char *evli = tovli(nl_l+namelen+id_l+parent_l+size_l+offset_l,&vli1_l);
	char *svli = tovli(mtime_l+ctime_l+atime_l+3,&vli2_l);
	zxr_write(W, evli, vli1_l);
	zxr_write(W, &ent->type, 1);
	zxr_write(W, id, id_l);
	zxr_write(W, parent, parent_l);
	if (ent->type == 1) {
		zxr_write(W, size, size_l);
		zxr_write(W, offset, offset_l);
	}
	zxr_write(W, svli, vli2_l);
	zxr_write(W, "\x03", 1);
	zxr_write(W, mtime, mtime_l);
	zxr_write(W, ctime, ctime_l);
	zxr_write(W, atime, atime_l);
	zxr_write(W, "l", 1);
	zxr_write(W, &ent->mode, 2);
	free(nl);
	free(id);
	free(parent);
	if (ent->type == 1) {
		free(size);
		free(offset);
	}
	free(mtime);
	free(ctime);
	free(atime);
	free(evli);
	free(svli);
}

void copy_file(zxr_hand *W, char *path) {
	FILE *f = fopen(path, "rb");
	size_t read_amt = 0;
	char *fb = malloc(W->chunk_size);
	size_t last_pos = 0;
	do {
		read_amt = fread(fb, W->chunk_size-W->pos, 1, f);
		size_t amt = ftell(f)-last_pos;
		last_pos = ftell(f);
		if (read_amt != W->chunk_size-W->pos) {
			if (ferror(f) && !feof(f)) {
				fprintf(stderr, "I/O Error\n");
				abort();
			}
		}
		//printf("read_amt: %lu\n%s\n", read_amt, fb);
		if (amt > 0)
			zxr_write(W, fb, amt);
	} while (read_amt != 0);
	fclose(f);
	free(fb);
}

void scan_dir(zxr_hand *W, DIR *dir, char * relpath, size_t parent) {
	struct dirent *ent;
	struct stat stbuf;
	do {
		ent = readdir(dir);
		if (ent != NULL && strcmp("..", ent->d_name) != 0 && strcmp(".", ent->d_name) != 0) {
			//if (W->verbose)
			//	printf("Found %s/%s.\n", relpath, ent->d_name);
			char *path = malloc(strlen(relpath)+strlen(ent->d_name)+2);
			sprintf(path, "%s/%s", relpath, ent->d_name);
			stat(path, &stbuf);
			zxrent *list = malloc(sizeof(zxrent)*(ents+1));
			memcpy(list, entries, sizeof(zxrent)*ents);
			list[ents].type = 0;
			if (S_ISREG(stbuf.st_mode)) {
				list[ents].type = 1;
			}
			list[ents].path = path;
			list[ents].parent = parent;
			list[ents].id = id;
			if (S_ISREG(stbuf.st_mode)) {
				list[ents].size = stbuf.st_size;
				list[ents].offset = offset;
			}
			list[ents].name = malloc(strlen(ent->d_name)+1);
			strcpy(list[ents].name, ent->d_name);
			ents++;
			free(entries);
			entries = list;
			id++;
			entries[ents-1].atime = stbuf.st_atime;
			entries[ents-1].mtime = stbuf.st_mtime;
			entries[ents-1].ctime = stbuf.st_ctime;
			entries[ents-1].mode = stbuf.st_mode & 0x9FF;
			write_entry(W, &entries[ents-1]);
			if (S_ISREG(stbuf.st_mode))
				offset+=stbuf.st_size;
			else if (S_ISDIR(stbuf.st_mode)) {
				DIR *scanning = opendir(path);
				scan_dir(W, scanning, path, id-1);
				closedir(scanning);
			}
		}
	} while (ent != NULL);
}

void make_zxr(char *dir, zxr_hand *W) {
	entries = malloc(sizeof(zxrent));
	zxr_writemeta(W);
	DIR *scanning = opendir(dir);
	scan_dir(W, scanning, dir, 0);
	closedir(scanning);
	zxr_write(W, "\x00\x7F", 2);
	for (unsigned long long i = 0; i < ents; ++i) {
		if (entries[i].type) {
			//if (W->verbose)
				//printf("Copying %s...\n", entries[i].name);
			copy_file(W, entries[i].path);
		}
	}
	zxr_flush(W);
	free(entries);
}