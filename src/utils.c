#include "utils.h"

char *tovli(uint64_t i, char *len) {
	unsigned long long tmp = 0;
	char *buffer;
	*len = 0;
	if (i < 0x7F) {
		len[0] = 1;
		buffer = malloc(1);
		buffer[1] = i;
		return buffer;
	}
	do {
		tmp |= 0x7F << (len[0]*7);
		len[0]++;
	} while (tmp < i);
	printf("len: %d, tmp: %llu\n", *len, tmp);
	buffer = malloc(len[0]);
	for (char j=0; j<*len; ++j) {
		buffer[j] = (i & (0x7F << (7*j))) >> (7*j);
		buffer[j] |= 0x80;
	}
	buffer[*len-1] &= 0x7F;
	return buffer;
}