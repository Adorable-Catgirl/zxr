#include "zxrio.h"
#include "utils.h"
#include "extensions.h"

void zxr_writemeta(zxr_hand * W) {
	fwrite("zxr\0\0\0\0\0", 8, 1, W->handle);
	fwrite("\0\0\0\0\0\0", 6, 1, W->handle);
	fwrite(&W->compressor->id, 1, 1, W->handle);
	fwrite(&W->checksum->id, 1, 1, W->handle);
	fwrite(&W->chunk_size, 8, 1, W->handle);
	W->compressor->init(W->level, W->chunk_size);
}

void _writechunk(zxr_hand *W) {
	char * sum = malloc(W->checksum->size*2);
	W->checksum->get_sum(W->buffer, W->chunk_size, sum);
	if (W->verbose) {
		char * debug = malloc((W->checksum->size*2)+1);
		memset(debug, 0, (W->checksum->size*2)+1);
		for (char i=0; i < W->checksum->size; ++i) {
				sprintf(debug, "%s%.2x", debug, sum[i]);
		}
		printf("Sum: %s\n", debug);
		free(debug);
	}
	char * dat = malloc(W->chunk_size*2);
	size_t len = W->compressor->compress(W->buffer, dat, W->chunk_size);
	if (W->verbose)
		printf("Compression: %llu -> %llu\n", W->chunk_size, len);
	char vli_l = 0;
	char * lenv = tovli(len, &vli_l);
	if (W->verbose) {
		char * debug = malloc((vli_l*2)+1);
		memset(debug, 0, (vli_l*2)+1);
		for (char i=0; i < vli_l; ++i) {
				sprintf(debug, "%s%.2x", debug, lenv[i]);
		}
		printf("Length VLI: %s\n", debug);
		free(debug);
	}
	fwrite(sum, W->checksum->size, 1, W->handle);
	fwrite(lenv, vli_l, 1, W->handle);
	fwrite(dat, len, 1, W->handle);
	//W->checksum->free(sum);
	free(dat);
	free(sum);
	free(lenv);
	W->pos = 0;
	memset(W->buffer, 0, W->chunk_size);
}

void _mcpy(void *to, void *from, size_t amt) {
	/*while (amt > 64*1024) {
		memcpy(to, from, 64*1024);
		amt -= 64*1024;
	}
	memcpy(to, from, amt);*/
	memcpy(to, from, amt);
}

void zxr_write(zxr_hand * W, void * buffer, uint32_t amt) {
	if (amt+W->pos < W->chunk_size) { 
		_mcpy(W->buffer+W->pos, buffer, amt);
		W->pos+=amt;
	} else if (amt+W->pos == W->chunk_size) {
		_mcpy(W->buffer+W->pos, buffer, amt);
		_writechunk(W);
	} else {
		_mcpy(W->buffer+W->pos, buffer, W->chunk_size-W->pos);
		size_t pos = W->pos;
		_writechunk(W);
		zxr_write(W, buffer+(W->chunk_size-pos), amt-(W->chunk_size-pos));
	}
}

void zxr_flush(zxr_hand * W) {
	_writechunk(W);
}

uint64_t fromvli(FILE *f) {
	uint64_t tmp = 0;
	char i = 0;
	char lc = 0;
	do {
		fread(&lc, 1, 1, f);
		printf("%x", lc);
		tmp |= (lc & 0x7F) << (7*i);
	} while (lc & 0x80);
	return tmp;
}

void _fastread(zxr_hand *W) { //This doesn't take time to calculate where it needs to seek.
	char * sum = malloc(W->checksum->size);
	fread(sum, W->checksum->size, 1, W->handle);
	printf("pos: %d\n", ftell(W->handle));
	uint64_t len = fromvli(W->handle);
	printf("%d\n", len);
	char * data = malloc(len);
	uint32_t dlen = W->compressor->decompress(data, W->buffer, len);
	if (dlen != W->chunk_size) {
		fprintf(stderr, "I/O Error: %d != %d\n", dlen, W->chunk_size);
		abort();
	} else if (!W->checksum->check(sum, W->buffer, W->chunk_size)) {
		fprintf(stderr, "I/O Error: Checksum mismatch.\n");
		abort();
	}
	W->last_chunk = W->pos/W->chunk_size;
}

void _readchunk(zxr_hand *W) {
	uint32_t chunkid = W->pos/W->chunk_size;
	fseek(W->handle, 24, SEEK_SET);
	W->pos = 0;
	while (chunkid) {
		--chunkid;
		fseek(W->handle, W->checksum->size, SEEK_CUR);
		uint64_t len = fromvli(W->handle);
		fseek(W->handle, len, SEEK_CUR);
		W->pos += W->chunk_size;
	}
	_fastread(W);
}

void zxr_read(zxr_hand * W, void * buffer, uint32_t amt) {
	if (amt == 0 || buffer == NULL) return;
	if (!(W->pos % W->chunk_size) && W->last_chunk==(W->pos/W->chunk_size)-1) {
		_fastread(W);
	} else if (!(W->pos % W->chunk_size)) {
		_readchunk(W);
	}
	if ((W->pos+amt)/W->chunk_size > W->last_chunk) {
		size_t pos = W->chunk_size - (W->pos % W->chunk_size);
		_mcpy(buffer, W->buffer+W->pos, pos);
		W->pos += pos;
		zxr_read(W, buffer+pos, amt-pos);
		return;
	}
	_mcpy(buffer, W->buffer+W->pos, amt);
	W->pos += amt;
}

void zxr_seek(zxr_hand * W, size_t pos) {
	W->pos = pos;
}

size_t zxr_pos(zxr_hand *W) {
	return W->pos;
}

void zxr_checktypes(zxr_hand *W) {
	fseek(W->handle, 0, SEEK_SET);
	uint32_t magic = 0;
	fread(&magic, 3, 1, W->handle);
	if (magic != 0x72787a) {
		fprintf(stderr, "I/O Error. Bad Magic (0x%.8x != 0x0072787a).\n", magic);
		exit(1);
	}
	fseek(W->handle, 11, SEEK_CUR);
	char csum_id, comp_id;
	fread(&comp_id, 1, 1, W->handle);
	fread(&csum_id, 1, 1, W->handle);
	fread(&W->chunk_size, 8, 1, W->handle);
	W->buffer = malloc(W->chunk_size);
	uint16_t extc = zxrext_count();
	for (uint16_t i=0; i<extc; ++i) {
		zxr_comp *as_comp = zxrext_getext(i);
		zxr_csum *as_csum = zxrext_getext(i);
		if (zxrext_gettype(i) == ZXR_EXT_COMPRESSOR && as_comp->id == comp_id) {
			W->compressor = zxrext_getext(i);
		}
		if (zxrext_gettype(i) == ZXR_EXT_CHECKSUM && as_csum->id == comp_id) {
			W->checksum = zxrext_getext(i);
		}
		if (W->compressor != NULL && W->checksum != NULL) {
			break;
		}
	}
	W->compressor->init(0, W->chunk_size);
}
