#ifndef __zxr_WRITER
#define __zxr_WRITER
#include <stdio.h>
#include <stdint.h>
#include <zxrext.h>

typedef struct {
	size_t chunk_size;
	size_t pos;
	size_t last_chunk;
	char level;
	zxr_comp * compressor;
	zxr_csum * checksum;
	char * buffer;
	FILE * handle;
	char verbose;
} zxr_hand;

void zxr_writemeta(zxr_hand * W);
void zxr_write(zxr_hand * W, void * buffer, uint32_t amt);
void zxr_flush(zxr_hand * W);
void zxr_read(zxr_hand * W, void * buffer, uint32_t amt);
void zxr_seek(zxr_hand *W, size_t pos);
size_t zxr_pos(zxr_hand *W);
void zxr_checktypes(zxr_hand *W);
#endif