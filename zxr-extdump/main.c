#include <stdlib.h>
#include <string.h>
#include <dlfcn.h>
#include <stdio.h>
#include <libgen.h>
#include <zxrext.h>

int main(int argc, char **argv) {
	void *lib = dlopen(argv[1], RTLD_NOW);
	if (lib == NULL) {
		printf("%s\n", dlerror());
		return 1;
	}
	char * objname = basename(argv[1]);
	char * libname = NULL;
	// find underscore
	for (char i = 0; i < strlen(objname); ++i) {
		if (objname[i] == '_') {
			libname = objname+i+1;
			break;
		}
	}
	if (libname == NULL) {
		printf("Library does not contain an info symbol.\n");
		return 1;
	}
	char * name = malloc(strlen(libname)-2);
	memcpy(name, libname, strlen(libname)-3);
	char * sym = malloc(strlen(name)+6);
	char * libsym = malloc(5);
	memcpy(libsym, objname, 4);
	sprintf(sym, "%s_info", name);
	char * libt = malloc(strlen(sym)+6);
	sprintf(libt, "%s_%s", name, libsym);
	printf("%s\n", objname);
	zxr_extinfo * info = dlsym(lib, sym);
	printf("Pointer: %p\n", info);
	printf("Name: %s\n", info->name);
	printf("Version: %s\n", info->version);
	printf("Author: %s\n", info->author);
	printf("Website: %s\n", info->website);
	printf("Bug Report Address: <%s>\n", info->bugreport);
	printf("\n\n");
	if (strcmp(libsym, "comp") == 0) {
		printf("Compressor extension\n");
		zxr_comp * comp = dlsym(lib, libt);
		printf("Pointer: %p\n", info);
		printf("Compressor pointer: %p\n", comp->compress);
		printf("Decompressor pointer: %p\n", comp->decompress);
		printf("Name: %s\n", comp->name);
		printf("ID: %d\n", comp->id);
	} else if (strcmp(libsym, "csum") == 0) {
		printf("Checksum extension\n");
		zxr_csum * csum = dlsym(lib, libt);
		printf("Pointer: %p\n", info);
		printf("Sumgen pointer: %p\n", csum->get_sum);
		printf("Sumval pointer: %p\n", csum->check);
		printf("Name: %s\n", csum->name);
		printf("ID: %d\n", csum->id);
		printf("Csum size: %d\n", csum->size);
		char test = 0;
		char * debug = malloc((csum->size*2)+1);
		char * sum = malloc(csum->size);
		csum->get_sum(NULL, 0, sum);
		for (char i=0; i < csum->size; ++i) {
			sprintf(debug, "%s%.2x", debug, sum[i]);
		}
		printf("Base sum: %s\n", debug);
		free(debug);
		//free(sum);
	} else {
		printf("Unknown type of extension.\n");
	}
	dlclose(lib);
	return 0;
}